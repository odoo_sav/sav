# -*- coding: utf-8 -*-
##############################################################################
#
#    SAV module for Odoo/OpenERP
#    Copyright (c) 2014 Mithril Informatique (http://www.mithril.re)
#    @author: Jean-Noël Rouchon <mail@mithril.re>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields
from openerp import api
from datetime import *
from dateutil.relativedelta import *

class sav_appareil(osv.Model):
  
  def _count_all(self, cr, uid, ids, field_name, arg, context=None):
    Intervention = self.pool['sav.intervention']
    return {
      appareil: {
        'intervention_count': Intervention.search_count(cr, uid, [('appareil_id', '=', appareil)], context=context),
      }
      for appareil in ids
    }

  def return_action_to_open(self, cr, uid, ids, context=None):
    if context is None:
      context = {}
    if context.get('xml_id'):
      res = self.pool.get('ir.actions.act_window').for_xml_id(cr, uid ,'sav', context['xml_id'], context=context)
      res['context'] = context
      res['domain'] = [('appareil_id','=', ids[0])]
      return res
    return False
 
  _name = 'sav.appareil'
  _description = 'Appareil'
  _columns = {
    'name' : fields.char('Nom', readonly=False),
    'serial': fields.many2one('stock.production.lot', 'Numero de serie', required=True),
    'product': fields.related('serial', 'product_id', type="many2one", relation='product.product', string="Article", store=True, select=True, readonly=True),
    'state': fields.selection([('stock', 'En stock'), ('client','Chez client'), ('sav', 'En SAV'), ('rebut', 'Au rebut')], 'Statut', readonly=True),
    'partner' :  fields.many2one('res.partner', 'Client'),
    'lieu' : fields.many2one('res.partner', 'Site'),
    'marque' : fields.char('Marque'),
    'modele' : fields.char('Modele'),
    'contract_id' : fields.many2one('account.analytic.account', 'Contrat'),
    'serie_interne' : fields.char('Numero de serie interne'),
    'date_vente' : fields.date('Date de vente'),
    'date_service' : fields.date('Date de mise en service'),
    'garantie_date_debut' : fields.date('Date de debut de garantie'),
    'garantie_date_fin' : fields.date('Date de fin de garantie'),
    'remplace' : fields.char('Remplace cet appareil'),
    'est_remplace' : fields.char('A ete remplace par cet appareil'),
    'notes' : fields.text('Notes'),
    'image': fields.binary("Image"),
    'log_interventions': fields.one2many('sav.intervention', 'appareil_id', 'Liste des interventions'),
    'intervention_count': fields.function(_count_all, type='integer', string='Interventions', multi=True),
    'facturable_mo': fields.selection([('100', 'Oui (100%)'), ('80','80%'), ('50', '50%'), ('Gratuit', 'Gratuit')], 'Main d oeuvre facturable ?'),
    'facturable_materiel': fields.selection([('100', 'Oui (100%)'), ('80','80%'), ('50', '50%'), ('Gratuit', 'Gratuit')], 'Materiel facturable ?'),
    'intervention_preventive_periodicite': fields.selection([('aucune', 'Aucune'), ('mensuel', 'Mensuel'), ('bimestriel', 'bimestriel'), ('trimestriel', 'Trimestriel'), ('semestriel', 'Semestriel'), ('annuel', 'Annuel')], 'Periodicite'),
    'intervention_preventive_date_debut' : fields.datetime('Date de debut'),
    'intervention_preventive_date_fin' : fields.date('Date de fin'),
    'intervention_ids': fields.one2many('sav.intervention', 'appareil_id', 'Interventions' ),
  }
  
  _defaults ={
    'facturable_mo': '100',
    'facturable_materiel': '100',
    'intervention_preventive_periodicite': 'aucune',
  }
  
  def on_change_serial(self, cr, uid, ids, serial, context=None):
    if not serial:
      return {}
    serie = self.pool.get('stock.production.lot').browse(cr, uid, serial, context=context)
    return {
      'value': {
        'product': serie.product_id,
        'image': serie.product_id.image_medium,
      }
    }
  
  def create(self, cr, uid, vals, context=None):
    if not vals['name']:
      serie = self.pool.get('stock.production.lot').browse(cr, uid, vals["serial"], context=context)
      vals['name'] = serie.product_id.name + ' - ' + serie.name
    return super(sav_appareil, self).create(cr, uid, vals, context=context)
    
  def on_change_periodicite(self, cr, uid, ids, datedeb, datefin, context=None):
    for appareil in self.browse(cr, uid, ids, context=context):
      contrat = self.pool.get('account.analytic.account').browse(cr, uid, appareil.contract_id, context=context)
      if not datedeb: 
      	datedeb = date.today()
      if not datefin:
      	datefin = date.today()
      return {
        'value': {
          'intervention_preventive_date_debut': datedeb,
          'intervention_preventive_date_fin': datefin,
        }
      }
    
  def preparer_preventives(self, cr, uid, ids, context=None):
    symptome = self.pool.get('sav.symptome').search(cr, uid, [('code', '=', 'V')])[0]
    for appareil in self.browse(cr, uid, ids, context=context):
      if appareil.intervention_preventive_periodicite and (appareil.intervention_preventive_periodicite != 'aucune'):
        month = 1
        if appareil.intervention_preventive_periodicite == 'mensuel':
          month = 1
        if appareil.intervention_preventive_periodicite == 'bimenstriel':
          month = 2
        elif appareil.intervention_preventive_periodicite == 'trimestriel':
          month = 3
        elif appareil.intervention_preventive_periodicite == 'semestriel':
          month = 6
        elif appareil.intervention_preventive_periodicite == 'annuel':
          month = 12
        else:
          month = 1
        date_deb = datetime.strptime(appareil.intervention_preventive_date_debut,"%Y-%m-%d %H:%M:%S")
        date_fin = datetime.strptime(appareil.intervention_preventive_date_fin,"%Y-%m-%d")
        date_interv = date_deb
        i = 0
        while date_interv <= date_fin:
          values = {
            'date_demande': date.today(),
            'date_intervention': date_interv,
            'state': 'bon',
            'preventive': True,
            'symptome_id': symptome,
            'demande_desc': 'Intervention préventive',
            'appareil_id': appareil.id
          }
          self.pool.get('sav.intervention').create(cr, uid, values)
          i = i + month
          date_interv = date_deb+relativedelta(months=i)
      
class sav_intervention(osv.Model):
  _name = 'sav.intervention'
  _description = 'Intervention'
  _columns = {
    'name': fields.char('Numero', readonly=True),
    'date_demande': fields.date('Date de demande', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'state': fields.selection([('demande', 'Demande'), ('bon','Bon d intervention'), ('rapport', 'Rapport d intervention'), ('facture', 'Tranfert en facture')], 'Statut'),
    'partner' :  fields.related('appareil_id', 'partner', type='many2one', relation='res.partner', readonly=True, string="Client"),
    'appareil_id' : fields.many2one('sav.appareil', 'Appareil', readonly=True, states={'demande': [('readonly', False)]}, required=True),
    'symptome_id' : fields.many2one('sav.symptome', 'Appareil', readonly=True, states={'demande': [('readonly', False)]}, required=True),
    'pannes_constatees' : fields.char('Pannes constatees', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'technicien' : fields.many2one('sav.technicien', 'Technicien', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_intervention': fields.datetime('Date d intervention prevue', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'intervention' : fields.text('Description de l intervention', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'numero_interne': fields.char('Numero interne', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'numero_rapport_manuel': fields.char('Numero manuel du rapport', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'intervention_line': fields.one2many('sav.intervention.line', 'intervention_id', 'Materiel et main d oeuvre', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'technicien_line': fields.one2many('sav.intervention.technicien', 'intervention_id', 'Techniciens', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'facturable_mo': fields.selection([('100', 'Oui (100%)'), ('80','80%'), ('50', '50%'), ('Gratuit', 'Gratuit')], 'Main d oeuvre facturable ?', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'facturable_materiel': fields.selection([('100', 'Oui (100%)'), ('80','80%'), ('50', '50%'), ('Gratuit', 'Gratuit')], 'Materiel facturable ?', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'demandeur': fields.char('Demandeur', readonly=True, states={'demande': [('readonly', False)]}),
    'type_intervention': fields.selection([('site', 'Site'), ('hors-site','Hors Site')], 'Type d intervention', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'demande_desc' : fields.text('Description de la demande', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}, required=True),
    'outil_id': fields.many2one('sav.outil', 'Utilisations d outils de controle', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'tests' : fields.char('Tests realises', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_prise_en_charge': fields.date('Prise en charge client', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_entree_atelier': fields.date('Entree en atelier', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_decontamination': fields.date('Decontamination', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_diagnostique': fields.date('Diagnostique', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'date_demande_devis': fields.date('Demande de devis', states={'rapport': [('readonly', True)], 'facture': [('readonly', True)]}),
    'garantie_date_fin': fields.date(related='appareil_id.garantie_date_fin', readonly=True, string="Date de fin de garantie"),
    'date_service' : fields.date(related='appareil_id.date_service', readonly=True, string='Date de mise en service'),
    'serial_id': fields.related('appareil_id', 'serial', type="many2one", relation='stock.production.lot', readonly=True, string="S/N"),
    'product_id': fields.related('appareil_id', 'product', type='many2one', relation='product.product', readonly=True, string="Produit"),
    'contract_id': fields.related('appareil_id', 'contract_id', type='many2one', relation='account.analytic.account', readonly=True, string="Contrat"),
    'observations' : fields.text('Observations'),
    'preventive': fields.boolean('Preventive ?'),
  }
  
  _defaults ={
    'state': 'demande',
    'preventive': False,
  }
  
  def to_bon(self, cr, uid, ids, context=None):
    return self.write(cr, uid, ids, {'state': 'bon'}, context=context)
  
  def to_rapport(self, cr, uid, ids, context=None):
    return self.write(cr, uid, ids, {'state': 'rapport'}, context=context)
  
  def to_facture(self, cr, uid, ids, context=None):
    
    for intervention in self.browse(cr, uid, ids, context=context):
      livraison = False
      for line_id in self.pool.get('sav.intervention.line').search(cr, uid, [('intervention_id', '=', intervention.id)]):
        line = self.pool.get('sav.intervention.line').browse(cr, uid, line_id, context=context)
        if not(line.service) and not(line.facturable=="Gratuit"):
          livraison = True
    
    	livraison = True #TODO Pour le moment, seul la commande est créée
      if livraison:
        self.create_saleorder(cr, uid, intervention.id, context)
      else:
        self.create_invoice(cr, uid, intervention.id, context)
        
    return self.write(cr, uid, ids, {'state': 'facture'}, context=context)
  
  def create_invoice(self, cr, uid, intervention_id, context=None):
    #TODO La TVA n'est pas prise en compte !
    intervention = self.pool.get('sav.intervention').browse(cr, uid, intervention_id, context=context)
    journal_ids = self.pool.get('account.journal').search(cr, uid, [('type', '=', 'sale')], limit=1)
    invoice_vals = {
      'name': '',
      'origin': intervention.name,
      'type': 'out_invoice',
      'reference': intervention.name,
      'account_id': intervention.partner.property_account_receivable.id,
      'partner_id': intervention.partner.id,
      'journal_id': journal_ids[0],
      'fiscal_position': intervention.partner.property_account_position.id,
      'user_id': uid,
      }
    facture = self.pool.get('account.invoice').create(cr, uid, invoice_vals)
    
    for line_id in self.pool.get('sav.intervention.line').search(cr, uid, [('intervention_id', '=', intervention.id)]):
      line = self.pool.get('sav.intervention.line').browse(cr, uid, line_id, context=context)
      remise = 0
      if line.facturable=='Gratuit':
        remise = 100
      else:
        remise = 100-int(line.facturable)
      invoice_line_vals = {
                           'invoice_id': facture,
                           'partner_id': intervention.partner.id,
                           'origin': intervention.name,
                           'product_id': line.product_id.id,
                           'name': line.name,
                           'quantity': line.quantity,
                           'price_unit':line.unit_price,
                           'discount': remise,
                           'price_subtotal': line.quantity*line.unit_price*remise/100,
                           }
      self.pool.get('account.invoice.line').create(cr, uid, invoice_line_vals)
      
    return facture
  
  def create_saleorder(self, cr, uid, intervention_id, context=None):
    intervention = self.pool.get('sav.intervention').browse(cr, uid, intervention_id, context=context)
    saleorder_vals = {
      'origin': intervention.name,
      'partner_id': intervention.partner.id,
      'state' : 'draft',
      'date_confirm' : date.today(),
      }
    saleorder = self.pool.get('sale.order').create(cr, uid, saleorder_vals)
    
    for line_id in self.pool.get('sav.intervention.line').search(cr, uid, [('intervention_id', '=', intervention_id)]):
      line = self.pool.get('sav.intervention.line').browse(cr, uid, line_id, context=context)
      remise = 0
      if line.facturable=='Gratuit':
        remise = 100
      else:
        remise = 100-int(line.facturable)
      order_line_vals = {
                           'order_id': saleorder,
                           'partner_id': intervention.partner.id,
                           'product_id': line.product_id.id,
                           'name': line.name,
                           'product_uos_qty': line.quantity,
                           'product_uom_qty': line.quantity,
                           'price_unit':line.unit_price,
                           'discount': remise,
                         }
      self.pool.get('sale.order.line').create(cr, uid, order_line_vals)
      
    return saleorder
    
  def on_change_appareil(self, cr, uid, ids, appareil_id, context=None):
    if not appareil_id:
      return {}
    appareil = self.pool.get('sav.appareil').browse(cr, uid, appareil_id, context=context)
    return {
      'value': {
        'partner': appareil.partner,
        'facturable_mo': appareil.facturable_mo,
        'facturable_materiel': appareil.facturable_materiel,
        'contract_id': appareil.contract_id,
        'product_id': appareil.product,
        'serial_id': appareil.serial,
      }
    }
  
  """
  Sucharge de la methode create pour ajouter la séquence dans le nom de l'intervention
  """
  def create(self, cr, uid, vals, context=None):
    if vals.get('name', '/') == '/':
      vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'sav.intervention') or '/'
    if not(vals.get('date_demande')):
      vals['date_demande'] = date.today()
    return super(sav_intervention, self).create(cr, uid, vals, context=context)

class sav_intervention_technicien(osv.Model):
  _name = 'sav.intervention.technicien'
  _description = 'Techniciens par intervention'
  _columns = {
    'intervention_id' : fields.many2one('sav.intervention', 'Intervention', required=True),
    'technicien_id': fields.many2one('sav.technicien', 'Technicien', required=True),
    'date_debut': fields.datetime('Date debut', required=True),
    'date_fin': fields.datetime('Date fin', required=True),
  }

class sav_intervention_ligne(osv.Model):
  _name = 'sav.intervention.line'
  _description = 'Ligne d intervention'
  _columns = {
    'intervention_id' : fields.many2one('sav.intervention', 'Intervention', required=True),
    'product_id': fields.many2one('product.product', 'Article', required=True),
    'product_type': fields.char('Type de produit'),
    'name': fields.text('Description'),
    'quantity' : fields.float('Quantite', required=True),
    'unit_price' : fields.float('Prix unitaire', required=True),
    'total_line' : fields.float('montant'),
    'facturable' : fields.selection([('100', 'Oui (100%)'), ('80','80%'), ('50', '50%'), ('Gratuit', 'Gratuit')], 'Facturable ?', readonly=False),
    'service' : fields.boolean("Service"),
  }
  
  _defaults ={
    'service': True,
  }
  
  def on_change_product(self, cr, uid, ids, product_id, context=None):
    if not product_id:
      return {}
    product = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
    #product_template = self.pool.get('product.template').browse(cr, uid, product.product_tmpl_id, context=context)
    return {
      'value': {
        'name': product.description_sale or product.name,
        'quantity': 1,
        'unit_price': product.lst_price,
        'total_line': product.lst_price,
        'service': product.type=='service'
      }
    }
  
  def on_change_quantity(self, cr, uid, ids, quantity, unit_price, context=None):
    total = quantity*unit_price
    return {
      'value': {
        'total_line': total,
      }
    }
  
  """
  Sucharge de la methode create pour remplir le champ facturable si il n'a pas été fait manuellement
  """
  def create(self, cr, uid, vals, context=None):
    line = super(sav_intervention_ligne, self).create(cr, uid, vals, context=context)
    for record in self.browse(cr, uid, line, context=context):
      if not(record.facturable):
        if record.product_id.type == 'service':
          self.write(cr, uid, record.id, {'facturable': record.intervention_id.appareil_id.facturable_mo, 'product_type': record.product_id.type,}, context=context)
        else:
          self.write(cr, uid, record.id, {'facturable': record.intervention_id.appareil_id.facturable_materiel, 'product_type': record.product_id.type,}, context=context)          
    return line  
  
class sav_technicien(osv.Model):
  _name = 'sav.technicien'
  _description = 'Technicien'
  _columns = {
    'name' : fields.char('Nom', required=True),
    'image_small' : fields.binary('Photo du technicien'),
  }
  
  _sql_constraints = [
    ('name_unique', 'unique(name)', 'Ce nom existe deja !'),
  ]
  
class sav_outil(osv.Model):
  _name = 'sav.outil'
  _description = 'Outil'
  _columns = {
    'name' : fields.char('Nom', required=True),
    'image_small' : fields.binary('Photo de l outil'),
  }
  
  _sql_constraints = [
    ('name_unique', 'unique(name)', 'Ce nom existe deja !'),
  ]
  
class sav_symptome(osv.Model):
  _name = 'sav.symptome'
  _description = 'Symptome'
  _columns = {
    'name' : fields.char('Nom', required=True),
    'code' : fields.char('Code'),
  }
  
  _sql_constraints = [
    ('code_unique', 'unique(code)', 'Ce code existe deja !'),
    ('name_unique', 'unique(name)', 'Ce nom existe deja !'),
  ]
  
  def name_search(self, cr, user, name='', args=None, operator='ilike', context=None, limit=100):
    if not args:
      args = []
    if name:
      ids = self.search(cr, user, ['|',('code',operator,name),('name',operator,name)] + args, limit=limit, context=context)
    else:
      ids = self.search(cr, user, args, limit=limit, context=context)
    result = self.name_get(cr, user, ids, context=context)
    return result
  
class stock_production_lot(osv.osv):
  _name = 'stock.production.lot'
  _inherit = 'stock.production.lot'
  
  """
  Sucharge de la methode create pour creer automatiquement un appareil
  """
  def create(self, cr, user, vals, context=None):      
    new_lot = super(stock_production_lot, self).create(cr, user, vals, context=context)
    values = {
      'serial': new_lot,
      'state': 'stock',
      'name': '',
    }
    self.pool.get('sav.appareil').create(cr, user, values)
    return new_lot


class stock_picking(osv.osv):
  _name = "stock.picking"
  _inherit = "stock.picking"
  
  #Sucharge de la methode do_transfer pour modifier automatiquement la fiche appareil
  @api.cr_uid_ids_context
  def do_transfer(self, cr, uid, picking_ids, context=None):
    update_move = super(stock_picking, self).do_transfer(cr, uid, picking_ids, context=context)
    pickings = self.pool.get('stock.picking').browse(cr, uid, picking_ids, context=context)
    for picking in pickings:
    
      #Si le mouvement est une sortie
      if picking.picking_type_id.id == 2:
        pack_operation_ids = self.pool.get('stock.pack.operation').search(cr, uid, [('picking_id', '=', picking.id)], context=context)
        pack_operations = self.pool.get('stock.pack.operation').browse(cr, uid, pack_operation_ids, context=context)
        for pack_operation in pack_operations:
          warranty = int(pack_operation.product_id.warranty)
          appareils_obj = self.pool.get('sav.appareil')
          appareils = appareils_obj.search(cr, uid, [('serial', '=', pack_operation.lot_id.id)], context=context)
          for appareil in appareils:
            date = datetime.strptime(picking.date,"%Y-%m-%d %H:%M:%S")
            date_fin_garantie = date+relativedelta(months=warranty)
            appareils_obj.write(cr, uid, appareil, {'partner': picking.partner_id.id, 
										                                'date_vente': picking.date, 
										                                'garantie_date_debut': picking.date, 
										                                'garantie_date_fin': date_fin_garantie,
										                                'date_service': picking.date,
										                                'state': 'client'}, context=context)
    return update_move

"""
Sucharge de la classe account_analytic_account (contrats) pour ajouter les liens vers les appareils et la politique de facturation
""" 
class account_analytic_account(osv.osv):
  _name = 'account.analytic.account'
  _inherit = 'account.analytic.account'
  
  _columns = {
    'appareil' : fields.one2many('sav.appareil', 'contract_id', 'Appareils'),
    'intervention_ids' : fields.one2many('sav.intervention', 'contract_id', 'Interventions'),
    
  }
