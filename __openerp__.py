{
    'name' : 'SAV',
    'version' : '0.5',
    'author' : 'Mithril Informatique',
    'sequence': 110,
    'category': 'Gérer la maintenance des appareils',
    'website' : 'https://www.mithril.re',
    'summary' : 'Maintenance, appareils, technicien',
    'description' : "Maintenance, appareils, technicien",
    'depends' : [
        'base',
        'sale',
    ],
    'data' : [
    		'security/sav_security.xml',
        'security/ir.model.access.csv',
        'sav_view.xml',
        'sav_report.xml',
        'sav_intervention_sequence.xml',
    ],

    'installable' : True,
    'application' : True,
}
